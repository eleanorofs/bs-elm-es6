# bs-elm-es6
## a package for interoperation between Elm and BuckleScript compatible with ES6 modules

## Philosophy
All respect to the existing `bs-elm` project, but since it required a script
bundler, I decided to write my own that doesn't require a bundler to fit my 
use case. 

## Installation
`npm install bs-elm-es6` in your console and then add `"bs-elm-es6"` to your
bs-dependencies. 

## Usage
1. *(Optional)* Expose the module in your Index.re, like so:
    open ElmES6;
    
2. Define your `ports` type as a record, where each field is either a 
`sendable` of a type or a `subscribable` of a type. For example: 
```
     type ports = 
     {
       toElm: Elm.sendable(string),
       fromElm: Elm.subscribable(string)
     };
```
2. Get your target node and initialize your app. For example: 
```
    [@bs.val][bs.scope "document"]
    external getElementById: string => Dom.element = "getElementById";
    let app: Elm.app(ports) = 
      Elm.Main.init({ node: getElementById("elm-target") });
```
3. Use your ports. There are two ways to do this. 

### The BuckleScript-y Way to use ports. 
`bs-elm-es6` exposes methods `Elm.send('model, sendable('model))` and 
`Elm.subscribe(('model => unit), subscribable('model))`. 

You can pipe into these methods like so:
```ocaml
app.ports.toElm -> Elm.send("Hello, from ReasonML.");
app.ports.fromElm -> Elm.subscribe(str => Js.log(str));
```
I imagine this is the primary way most users will interact with this package.

### The JavaScript-y Way to use ports.
`bs-elm-es6` also exposes a less-idiomatic-to-BuckleScript way to interact with
Elm ports. That is, `sendable('model)` and `subscribable('model)` are both 
records with a function-valued field. 

That is, you can literally call the object method directly in ReasonML like: 
    app.ports.toElm.send("Hello, from ReasonML");
    
This is kind of a cringe-y hack and I don't imagine it will be widely used, but
it may be useful if you're coming from the JavaScript/Elm world or if you want
a faster refactor from JavaScript to Reason. (Plus, I really just wanted to 
see if 
something like this would work, and it does.)

## Example
There's a fully functioning, minimal example at 
https://elmandbucklescript.bitbucket.io which includes one incoming and one 
outgoing port. Let me know if you have any questions.

## For further reading
I *strongly* recommend you check out my 
[catch-all documentation](https://webbureaucrat.gitlab.io/posts/issues-and-contribution/)
on my 
projects. It describes how to get in touch with me if you have any questions, 
how to contribute code, coordinated disclosure of security vulnerabilities, 
and more. It will be regularly updated with any information I deem relevant
to my side projects. 

Most of all, feel free to get in touch! I'm delighted to hear suggestions
or field questions. 
